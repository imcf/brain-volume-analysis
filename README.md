# Brain volume analysis

- helper scripts used in the IMCF workflow to register and analyse 3D volumes of mouse brain.
- the workflow is documented on the Biozentrums internal wiki space: https://wiki.biozentrum.unibas.ch/x/xEBwEw

## median.ijm
- IJ-macro. Applies a 15px median filter to an image and saves the resulting image

## remap_labelimage.py
- Fiji macro to workaround a bug in Imaris measurement
- re-maps the reference atlas label image

## atlas-analysis.ipynb
juypter notebook example of secondary analysis:
- label image intensity to atlas region mapping
- aggregation of object frequency

# License
GNU GPLv3
