#@ File inputimage
#@ File (style="directory") target


open(inputimage);
run("Median...", "radius=15");
title = getTitle();
path = target + "/" + title;
print(path);
saveAs("Tiff", path);
run("Close All");

